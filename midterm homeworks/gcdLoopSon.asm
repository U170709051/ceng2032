	.data
	
firstNumber: .asciiz "Please enter an integer: "

secondNumber: .asciiz "\nPlease enter another integer: "

result: .asciiz "\nThe GCD is: "

newline: .asciiz "\n"

.align 2

.globl main

.text

main:

#Request first integer from the user
    la $a0, firstNumber #load adress
    
    li $v0, 4      # $v0 = 4 ("load immediate")   
          
    syscall                

    li $v0, 5        # $v0 = 5 ("load immediate")   
      
    syscall    
                
    add $a1, $v0, $zero    

#Request second integer from the user
    la $a0, secondNumber    
           
    li $v0, 4      
            
    syscall                

    li $v0, 5   
               
    syscall   
                 
    add $a2, $v0, $zero    

#call gcdLoop with two paremeters

    addi $sp, $sp, -8 
         
    sw $a2, 0($sp) 
            
    sw $a1, 4($sp)    
 
    
                  
    jal Loop   
          
    lw $a1, 4($sp) 
                 
    lw $a2, 0($sp)       
                     
    addi $sp, $sp, 4   
        
    add $s0, $v0, $zero   
     
    sw $s0, 0($sp)     
        

#get the result from the gcdLoop and print it
            
    li $v0, 4
    
    la $a0, result   
                  
    syscall   
                               
    li $v0, 1  
                
    add $a0, $s0, $zero   
     
    syscall                
     
    la $a0, newline  
          
    li $v0, 4     
             
    syscall       
                              
    li $v0, 10  
               
    syscall                



#find the gcd of the given two integers recursively

Loop:
    beq $a1, $a2, .L2 
    
    sgt $v0, $a2, $a1 
    
    bne $v0, $zero, .L1
     
    subu $a1, $a1, $a2 #substract
    
    b Loop
    
.L1:
    subu $a2, $a2, $a1 
    
    b Loop
.L2:
    move $v0, $a1 #move to be printed
    
    jr $ra #jump register
    
